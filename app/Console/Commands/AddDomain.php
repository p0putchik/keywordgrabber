<?php

namespace App\Console\Commands;

use App\Core\Domain\Service\DomainService;
use App\Core\Url\Service\UrlService;
use Illuminate\Console\Command;

class AddDomain extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "grabber:addDomain 
        {domain : domain name} 
        {lang : lang name (example: en)} 
        {--url=/ : url(default '/')}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Add domain with lang and default url";

    protected $domainService;
    protected $urlService;

    /**
     * Create a new command instance.
     * @param DomainService $domainService
     * @param UrlService $urlService
     */
    public function __construct(DomainService $domainService, UrlService $urlService)
    {
        parent::__construct();

        $this->domainService = $domainService;
        $this->urlService = $urlService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $args = $this->arguments();
        $opts = $this->options();
        $domain = $args['domain'] ?? null;
        if (empty($domain)) {
            exit('Empty domain');
        }

        $lang = $args['lang'] ?? null;
        if (empty($lang)) {
            exit('Empty Lang');
        }

        $url = $opts['url'] ?? null;
        if (empty($url)) {
            exit('Empty url');
        }

        $this->domainService->addDomain($domain, $lang);
        $this->urlService->addUrl($url, $domain);

        $this->info(sprintf("Add domain '%s' with lang '%s' and url '%s'",
            $domain,
            $lang,
            $url
        ));
    }
}
