<?php

namespace App\Core\Url\Service;


use App\Core\Domain\Repository\DomainRepository;
use App\Core\Url\Dto\UrlDto;
use App\Core\Url\Exception\UrlAddException;
use App\Core\Url\Repository\UrlRepository;

class UrlService
{
    protected $domainRepository;
    protected $urlRepository;

    /**
     * UrlService constructor.
     * @param DomainRepository $domainRepository
     * @param UrlRepository $urlRepository
     */
    public function __construct(DomainRepository $domainRepository, UrlRepository $urlRepository)
    {
        $this->domainRepository = $domainRepository;
        $this->urlRepository = $urlRepository;
    }

    /**
     * @param string $name
     * @param string $domain
     * @return bool
     * @throws UrlAddException
     */
    public function addUrl(string $name, string $domain): bool
    {
        if (empty($name) || empty($domain)){
            throw new UrlAddException(sprintf("Error add url. Empty url(%s) or domain(%s)", $name, $domain));
        }

        $domainInfo = $this->domainRepository->getDomainInfo($domain);

        if (empty($domainInfo)){
            throw new UrlAddException(sprintf('Invalid domain - %s',$domain));
        }

        if ($this->urlRepository->isUrlExist($name, $domainInfo[0]->id)){
            return false;
        }

        return $this->urlRepository->addUrl($name, $domainInfo[0]->id);
    }

    /**
     * @param string $domain
     * @param int $count
     * @return array UrlDto
     * @throws UrlAddException
     */
    public function getNotProcessedUrls(string $domain, int $count) : array
    {
        $domainInfo = $this->domainRepository->getDomainInfo($domain);

        if(!isset($domainInfo[0])){
            throw new UrlAddException(sprintf("Domain %s not found", $domain));
        }

        $urlsCollection = $this->urlRepository->getNotProcessedUrls($domainInfo[0]->id, $count);

        $dtoArray = [];
        foreach ($urlsCollection as $item){
            $dtoArray[] = new UrlDto(
                $item->id,
                $item->name,
                $item->domain_id,
                $item->processed
                );
        }

        return $dtoArray;
    }

    /**
     * @param string $name
     * @param string $domain
     * @throws UrlAddException
     */
    public function markUrlAsProcessed(string $name, string $domain){
        $domainInfo = $this->domainRepository->getDomainInfo($domain);

        if(!isset($domainInfo[0])){
            throw new UrlAddException(sprintf("Domain %s not found", $domain));
        }

        $this->urlRepository->markUrlAsProcessed($name, $domainInfo[0]->id);
    }
}