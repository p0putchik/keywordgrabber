<?php

namespace App\Core\Domain\Dto;

class DomainDto
{
    protected $name;
    protected $lang;
    protected $comments;

    /**
     * DomainDto constructor.
     * @param int $id
     * @param string $name
     * @param string $lang
     * @param string $comments
     */
    public function __construct(int $id, string $name, string $lang, string $comments)
    {
        $this->id = $id;
        $this->name = $name;
        $this->lang = $lang;
        $this->comments = $comments;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getComments(): ?string
    {
        return $this->comments;
    }

    /**
     * @param string $comments
     */
    public function setComments(string $comments)
    {
        $this->comments = $comments;
    }

    /**
     * @return string
     */
    public function getLang(): ?string
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     */
    public function setLang(string $lang)
    {
        $this->lang = $lang;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }


}