<?php

namespace App\Core\Domain\Model;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{

    public function urls()
    {
        return $this->hasMany('App\Core\Url\Model\Url');
    }

    public function lang()
    {
        return $this->belongsTo('App\Core\Lang\Model\Lang');
    }
}
