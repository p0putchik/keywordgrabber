<?php

namespace App\Core\Keyword\Model;

use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    public function lang()
    {
        return $this->belongsTo('App\Core\Lang\Model\Lang');
    }

    public function domain(){
        return $this->belongsTo('App\Core\Domain\Model\Domain');
    }
}
