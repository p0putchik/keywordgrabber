<?php

namespace App\Core\Parser\Interfaces;


interface KeywordParser
{
    public function parseKeyword(string $content): array;
}