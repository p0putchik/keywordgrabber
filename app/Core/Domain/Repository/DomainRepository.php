<?php

namespace App\Core\Domain\Repository;


use App\Core\Domain\Model\Domain;
use Illuminate\Database\Eloquent\Collection;

class DomainRepository
{
    /**
     * @param Domain $domain
     * @return Collection
     */
    public function getAll(Domain $domain): Collection
    {
        return $domain::with('lang')->get();
    }

    /**
     * @param string $domain
     * @return Collection|static[]
     */
    public function getDomainInfo(string $domain)
    {
        return Domain::with('lang')->where('name', $domain)->get();
    }

    /**
     * @param string $domain
     * @param bool $onlyNotProcessed
     * @return Collection|static[]
     */
    public function getDomainUrl(string $domain, bool $onlyNotProcessed = false)
    {
        if (!$onlyNotProcessed) {
            return Domain::with('urls')->where('name', $domain)->get();
        }

        return Domain::with(['urls' => function ($query) {
            $query->where('processed', 0);
        }])->where('name', $domain)->get();
    }

    /**
     * @param string $name
     * @param int $langId
     * @param string $comments
     */
    public function addDomain(string $name, int $langId, string $comments = ''): void
    {
        $domain = new Domain();
        $domain->name = $name;
        $domain->lang_id = $langId;
        $domain->comments = $comments;
        $domain->save();
    }

    /**
     * @param string $name
     */
    public function removeDomain(string $name): void
    {
        $domain = Domain::where('name', $name)->get();
        $domain[0]->delete();
    }
}