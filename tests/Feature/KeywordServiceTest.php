<?php

namespace Tests\Feature;

use App\Core\Keyword\Service\KeywordService;
use Tests\TestCase;

class KeywordServiceTest extends TestCase
{
    /**
     * @dataProvider getLangNamesDataProvider
     * @param string $langName
     * @internal param int $langId
     * @internal param string $domain
     * @internal param bool $onlyProcessed
     */
    public function testGetKeywordsForLang(string $langName): void
    {
        $keywordService = $this->app->make(KeywordService::class);
        $keywords = $keywordService->getKeywordsForLang($langName);

        $this->assertTrue((bool)count($keywords));
    }

    /**
     * @return array
     */
    public function getLangNamesDataProvider(): array
    {
        return [
            ['ru'],
            ['en'],
        ];
    }

    public function testAddDeleteKeyword()
    {
        $keywordService = $this->app->make(KeywordService::class);
        $keyword = uniqid('keyword_', true);
        $lang = 'en';

        $result = $keywordService->addKeyword($keyword, $lang, 0);
        $isKeywordExist = $keywordService->isExistKeyword($keyword, $lang);
        $this->assertTrue($result);
        $this->assertTrue($isKeywordExist);

        $result = $keywordService->deleteKeyword($keyword, $lang);
        $this->assertTrue($result);
    }
}
