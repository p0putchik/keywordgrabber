<?php

namespace Tests\Feature;

use App\Core\Parser\Factory\BasicParsersFactory;
use App\Core\Parser\Interfaces\AbstractParsersFactory;
use App\Core\SiteGrabber\Service\BasicSiteGrabber;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class BasicGrabberTest extends TestCase
{
    /**
     * @param string $domain
     * @dataProvider getDomain
     */
    public function testGetUrls(string $domain)
    {
        $url = 'http://inovinky.eu/26481-illinois-datingside/';
        $d = file_get_contents($url);
        exit;

        $this->app->bind(AbstractParsersFactory::class, BasicParsersFactory::class);

        $basicGrabber = $this->app->make(BasicSiteGrabber::class);
        $basicGrabber->init($domain);
//        $urls = $basicGrabber->getUrls($basicGrabber->domain, 1);
//
//        $this->assertTrue((bool)count($urls));
//
//        $content = $basicGrabber->getUrlContent('/stroitelstvo-karkasnogo-doma/lestnicy-iz-metalla-svoimi-rukami');
//        $this->assertNotNull($content);
//
//        $basicGrabber->parsePageContent($content);
        $basicGrabber->parseUrls();
        $this->assertTrue(true);
    }

    public function getDomain()
    {
        return [
            ['trucktv.eu']
        ];
    }

}
