<?php

namespace App\Core\Parser\Factory;


use App\Core\Parser\Interfaces\AbstractParsersFactory;
use App\Core\Parser\Interfaces\KeywordParser;
use App\Core\Parser\Interfaces\LinkParser;
use App\Core\Parser\KeywordParsers\BasicKeywordParser;
use App\Core\Parser\LinkParsers\BasicLinkParser;

class BasicParsersFactory implements AbstractParsersFactory
{

    /**
     * @param string $domain
     * @param string $domainPrefix
     * @return LinkParser
     */
    public function createLinkParser(string $domain, string $domainPrefix): LinkParser
    {
        return new BasicLinkParser($domain, $domainPrefix);
    }

    /**
     * @param string $domain
     * @return KeywordParser
     */
    public function createKeywordParser(string $domain): KeywordParser
    {
        return new BasicKeywordParser( $domain);
    }
}