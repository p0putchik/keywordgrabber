<?php

namespace App\Core\Lang\Service;


use App\Core\Lang\Exception\AddLangException;
use App\Core\Lang\Exception\InvalidLangException;
use App\Core\Lang\Repository\LangRepository;

class LangService
{
    protected $langRepository;

    /**
     * LangService constructor.
     * @param $langRepository
     */
    public function __construct(LangRepository $langRepository)
    {
        $this->langRepository = $langRepository;
    }

    /**
     * @param string $name
     * @throws AddLangException
     */
    public function addLang(string $name): void
    {
        if (\strlen($name) > 2) {
            throw new AddLangException('Used more than 2 char in lang name');
        }

        $this->langRepository->addLang($name);
    }


    /**
     * @param string $name
     * @return
     * @throws InvalidLangException
     */
    public function getLangIdByName(string $name)
    {
        $lang = $this->langRepository->getLangIdByName($name);
        if (!\count($lang)){
            throw new InvalidLangException(sprintf('Invalid lang name %s', $name));
        }

        return $lang[0]->id;
    }
}