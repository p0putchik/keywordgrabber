<?php

namespace App\Core\Keyword\Dto;


class KeywordDto
{
    protected $name;
    protected $domain;
    protected $lang;

    /**
     * KeywordDto constructor.
     * @param $name
     * @param $domain
     * @param $lang
     */
    public function __construct(string $name, string $domain, string $lang)
    {
        $this->name = $name;
        $this->domain = $domain;
        $this->lang = $lang;
    }
}