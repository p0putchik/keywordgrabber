<?php

namespace App\Core\Domain\Service;

use App\Core\Domain\Dto\DomainDto;
use App\Core\Domain\Exception\NotExistDomainException;
use App\Core\Domain\Exception\RemoveDomainException;
use App\Core\Domain\Model\Domain;
use App\Core\Domain\Repository\DomainRepository;
use App\Core\Lang\Model\Lang;
use App\Core\Lang\Repository\LangRepository;
use App\Core\Url\Dto\UrlDto;
use App\Core\Url\Repository\UrlRepository;

class DomainService
{
    /**
     * @var DomainRepository
     */
    protected $domainRepository;

    protected $langRepository;

    protected $urlRepository;

    /**
     * DomainService constructor.
     * @param DomainRepository $domainRepository
     * @param LangRepository $langRepository
     * @param UrlRepository $urlRepository
     */
    public function __construct(DomainRepository $domainRepository, LangRepository $langRepository, UrlRepository $urlRepository)
    {
        $this->domainRepository = $domainRepository;
        $this->langRepository = $langRepository;
        $this->urlRepository = $urlRepository;
    }

    /**
     * @return array
     */
    public function getAllDomains(): array
    {
        $domainModel = new Domain();
        $domains = $this->domainRepository->getAll($domainModel);

        $domainsArray = [];
        foreach ($domains as $domain) {
            $domainsArray[] = new DomainDto(
                $domain->id,
                $domain->name,
                $domain->lang->name,
                $domain->comments
            );
        }

        return $domainsArray;
    }

    /**
     * @param string $domainName
     * @return DomainDto
     * @throws NotExistDomainException
     */
    public function getDomainInfo(string $domainName): DomainDto
    {
        $domains = $this->domainRepository->getDomainInfo($domainName);

        if (!\count($domains)) {
            throw new NotExistDomainException(sprintf('Invalid domain %s', $domainName));
        }

        return new DomainDto(
            $domains[0]->id,
            $domains[0]->name,
            $domains[0]->lang->name,
            $domains[0]->comments
        );
    }

    /**
     * @param string $domainName
     * @param bool $onlyNotProcessed
     * @return array
     */
    public function getDomainUrls(string $domainName, bool $onlyNotProcessed = false): array
    {
        $domainName = $this->domainRepository->getDomainUrl($domainName, $onlyNotProcessed);

        if (!isset($domainName[0])) {
            return [];
        }

        $urls = $domainName[0]->urls;
        if (!isset($urls[0])) {
            return [];
        }

        $urlsArray = [];
        foreach ($urls as $url) {
            $urlsArray[] = new UrlDto(
                $url->name,
                $url->domain_id,
                $url->processed
            );
        }

        return $urlsArray;
    }

    /**
     * @param string $domainName
     * @param string $lang
     * @internal param LangRepository $langRepository
     */
    public function addDomain(string $domainName, string $lang): void
    {
        $langInstance = Lang::where('name', $lang)->get();

        if (!count($langInstance)) {
            $this->langRepository->addLang($lang);
            $langInstance = Lang::where('name', $lang)->get();
        }

        $this->domainRepository->addDomain($domainName, $langInstance[0]->id);
    }

    /**
     * @param string $domainName
     * @throws RemoveDomainException
     */
    public function removeDomain(string $domainName): void
    {
        $domain = Domain::where('name', $domainName)->get();

        if (!\count($domain)) {
            throw new RemoveDomainException(sprintf('Invalid domain %s', $domainName));
        }

        $this->domainRepository->removeDomain($domainName);
    }

    public function getDomainWithNotProcessedUrl(): string
    {
        $url = $this->urlRepository->getDomainWithNotProcessedUrls();
        $domain = $url->domain->name;

        if(empty($domain)){
            throw new NotExistDomainException('All url processed');
        }

        return $domain;
    }
}