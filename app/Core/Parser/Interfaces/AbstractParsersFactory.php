<?php

namespace App\Core\Parser\Interfaces;


interface AbstractParsersFactory
{
    /**
     * @param string $domain
     * @param string $domainPrefix
     * @return LinkParser
     */
    public function createLinkParser(string $domain, string $domainPrefix): LinkParser;

    /**
     * @param string $domain
     * @return KeywordParser
     */
    public function createKeywordParser(string $domain): KeywordParser;

}