<?php

namespace App\Http\Controllers;

use App\Core\Domain\Model\Domain;
use App\Core\Domain\Service\DomainService;

class DomainGrabber extends Controller
{
    public function showAllDomains(DomainService $domainService)
    {
        echo "All Domains";
        var_dump($domainService->getAllDomains());
    }

    public function showDomainsUrl(DomainService $domainService, string $domain)
    {
        echo sprintf("URLs for domain %s%s", $domain, PHP_EOL);
        var_dump($domainService->getAllDomains());
    }

    public function showDomainsInfo(DomainService $domainService)
    {
        echo "Domains Info".PHP_EOL;
        var_dump($domainService->getAllDomains());
    }

    public function showDomainUrls(DomainService $domainService, string $domain)
    {
        echo "Domain Url".PHP_EOL;
        var_dump($domainService->getDomainUrls($domain));
    }
}
