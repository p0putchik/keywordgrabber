<?php

namespace App\Core\Lang\Model;


use Illuminate\Database\Eloquent\Model;

class Lang extends Model
{
    public function domains()
    {
        $this->hasMany('App\Core\Domain\Model');
    }

    public function keywords()
    {
        $this->hasMany('App\Core\Keyword\Model');
    }
}