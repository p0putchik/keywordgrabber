<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AddKeywords extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $langs = DB::table('langs')->get();
        $domains = DB::table('domains')->get();

        for($i=0;$i<5;$i++){
            $domainId = $domains[rand(0, count($domains)-1)]->id;
            $langId = $domains[rand(0, count($langs)-1)]->id;


            DB::table('keywords')->insert([
                'name' => sprintf("key_%s",uniqid()),
                'domain_id' => $domainId,
                'lang_id' => $langId,
                'created_at' => date("Y-m-d")
            ]);
        }
    }
}
