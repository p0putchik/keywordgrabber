<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AddDomains extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $langs = DB::table('langs')->get();
        $domains = DB::table('domains')->get();

        if (empty($domains)) {
            DB::table('domains')->insert([
                'name' => 'test1.ru',
                'lang' => $langs[rand(0,count($langs)-1)]->id,
                'comments' => 'comment1',
                'created_at' => date("Y-m-d")
            ]);

            DB::table('domains')->insert([
                'name' => 'test.com',
                'lang' => $langs[rand(0,count($langs)-1)]->id,
                'comments' => 'comment2',

                'created_at' => date("Y-m-d")
            ]);
        } else {
            foreach ($domains as $domain){
                $id = (int)$langs[rand(0,count($langs)-1)]->id;
                DB::table('domains')->where('id',$domain->id)->update(['lang_id' => $id]);
            }
        }
    }
}
