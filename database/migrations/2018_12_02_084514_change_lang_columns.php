<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeLangColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('keywords', function (Blueprint $table) {
            $table->dropUnique('keywords_name_lang_unique');
            $table->dropColumn('lang');
            $table->integer('lang_id')->unsigned()->nullable();
            $table->foreign('lang_id')
                ->references('id')->on('langs')
                ->onDelete('cascade');
        });

        Schema::table('domains', function (Blueprint $table) {
            $table->dropUnique('domains_name_lang_unique');
            $table->dropColumn('lang');
            $table->integer('lang_id')->unsigned()->nullable();
            $table->foreign('lang_id')
                ->references('id')->on('langs')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
