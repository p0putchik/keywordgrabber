<?php

namespace App\Core\Keyword\Service;

use App\Core\Keyword\Repository\KeywordRepository;
use App\Core\Lang\Exception\AddKeywordException;
use App\Core\Lang\Service\LangService;
use Mockery\Exception;

class KeywordService
{
    protected $keywordRepository;
    protected $langService;

    /**
     * KeywordService constructor.
     * @param $keywordRepository
     * @param $langService
     */
    public function __construct(KeywordRepository $keywordRepository, LangService $langService)
    {
        $this->keywordRepository = $keywordRepository;
        $this->langService = $langService;
    }

    /**
     * @param string $lang
     * @return array
     */
    public function getKeywordsForLang(string $lang): array
    {
        $keywords = $this->keywordRepository->getKeywordsForLang($lang);

        $returnArray = [];
        foreach ($keywords as $keyword) {
            $returnArray[] = $keyword->name;
        }

        return $returnArray;
    }

    /**
     * @param string $keyword
     * @param string $lang
     * @return bool
     */
    public function isExistKeyword(string $keyword, string $lang)
    {
        return $this->keywordRepository->isExistKeyword($keyword, $lang);
    }

    /**
     * @param string $keyword
     * @param string $lang
     * @param int|null $domainId
     * @return bool
     * @throws AddKeywordException
     */
    public function addKeyword(string $keyword, string $lang, int $domainId = null): bool
    {
        try {
            $langId = $this->langService->getLangIdByName($lang);
        } catch (Exception $e) {
            throw new AddKeywordException(sprintf('Add keyword Error -> %s', $e->getMessage()));
        }

        if (empty($keyword) || $this->isExistKeyword($keyword, $lang)) {
            return false;
        }

        $keyword = trim(mb_strtolower($keyword));
        $this->keywordRepository->addKeyword($keyword, $langId, $domainId);

        return true;
    }

    /**
     * @param string $keyword
     * @param string $lang
     * @return bool
     */
    public function deleteKeyword(string $keyword, string $lang): bool
    {
        $this->keywordRepository->deleteKeyword($keyword, $lang);

        return true;
    }
}