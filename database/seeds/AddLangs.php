<?php

use App\Core\Lang\Model\Lang;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AddLangs extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $langArray = [
            1 =>'ru',
            2 =>'en'
        ];

        foreach ($langArray as $id=>$name){
            $curRecord = DB::table('langs')->find($id);
            if ($curRecord === null){
                DB::table('langs')->insert([
                    'id' => $id,
                    'name' => $name
                ]);
            } else {
                $curRecord->id = $id;
                $curRecord->name = $name;
                $curRecord->save();
            }
        }
    }
}
