<?php

namespace App\Core\SiteGrabber\Service;


use App\Core\Domain\Dto\DomainDto;
use App\Core\Domain\Service\DomainService;
use App\Core\Keyword\Service\KeywordService;
use App\Core\Lang\Service\LangService;
use App\Core\Parser\Interfaces\AbstractParsersFactory;
use App\Core\Parser\Interfaces\HandlerTypes;
use App\Core\Parser\Interfaces\KeywordParser;
use App\Core\Parser\Interfaces\LinkParser;
use App\Core\SiteGrabber\Exception\SiteGrabberException;
use App\Core\SiteGrabber\Interfaces\SiteGrabberInterface;
use App\Core\Url\Service\UrlService;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

abstract class SiteGrabber implements SiteGrabberInterface
{
    /**
     * @var
     */
    public $domain;
    /**
     * @var
     */
    protected $domainPrefix;
    /**
     * @var DomainDto
     */
    protected $domainInfo;
    /**
     * @var
     */
    protected $domainService;
    /**
     * @var
     */
    protected $langService;
    /**
     * @var
     */
    protected $keywordService;
    /**
     * @var
     */
    protected $urlService;

    /**
     * @var AbstractParsersFactory
     */
    protected $parsersFactory;
    /**
     * @var LinkParser
     */
    protected $linkParser;

    /**
     * @var KeywordParser
     */
    protected $keywordParser;

    /**
     * SiteGrabber constructor.
     * @param DomainService $domainService
     * @param LangService $langService
     * @param KeywordService $keywordService
     * @param UrlService $urlService
     * @param AbstractParsersFactory $parsersFactory
     * @internal param $domain
     */
    public function __construct(
        DomainService $domainService,
        LangService $langService,
        KeywordService $keywordService,
        UrlService $urlService,
        AbstractParsersFactory $parsersFactory
    )
    {
        $this->domainService = $domainService;
        $this->langService = $langService;
        $this->keywordService = $keywordService;
        $this->urlService = $urlService;
        $this->parsersFactory = $parsersFactory;
    }

    /**
     * @param string $domain
     * @param string $domainPrefix
     * @throws SiteGrabberException
     */
    public function init(string $domain, string $domainPrefix = 'http://'): void
    {
        $this->domain = $domain ?? $this->getDomain();
        if (empty($this->domain)) {
            throw new SiteGrabberException('Empty domain');
        }
        $this->domainPrefix = $domainPrefix;

        $this->domainInfo = $this->domainService->getDomainInfo($domain);
        $this->keywordParser = $this->parsersFactory->createKeywordParser($domain);
        $this->linkParser = $this->parsersFactory->createLinkParser($domain, $domainPrefix);
    }

    /**
     * @return string
     * @throws \App\Core\Domain\Exception\NotExistDomainException
     */
    public function getDomain(): string
    {
        return $this->domainService->getDomainWithNotProcessedUrl();
    }

    public function parseUrls(array $urlsArray = null)
    {
        $urlsArray = $urlsArray ?? $this->getUrls($this->domain);

        if (empty($urlsArray)) {
            throw new SiteGrabberException('Parse Urls. Empty urlsArray');
        }

        foreach ($urlsArray as $url) {
            if (empty($url->getName())) {
                continue;
            }

            Log::info(sprintf('Domain %s. Parse url: %s',
                $this->domain,
                $url->getName()
            ));

            try {
                $content = $this->getUrlContent($url->getName());
            } catch (\Exception $e) {
                Log::error(sprintf("Domain %s. Failed get content for url %s with message %s",
                    $this->domain,
                    $url->getName(),
                    $e->getMessage()
                ));
            } finally {
                $this->urlService->markUrlAsProcessed($url->getName(), $this->domain);
            }

            if (empty($content)) {
                Log::error(sprintf("Domain %s. Invalid content for url %s",
                    $this->domain,
                    $url->getName()
                ));
                continue;
            }

            try {
                $this->parsePageContent($content);
            } catch (\Exception $e) {
                Log::error(sprintf("Domain %s. Parse error for url %s with message %s",
                    $this->domain,
                    $url->getName(),
                    $e->getMessage()
                ));
            }
        }
    }

    /**
     * @param string $domain
     * @param int $count
     * @return array|string
     * @throws \App\Core\Url\Exception\UrlAddException
     */
    public function getUrls(string $domain, int $count = null): array
    {
        $count = $count ?? Config::get('domaingrabber.urlProcessCount');

        return $this->urlService->getNotProcessedUrls($domain, $count);
    }

    /**
     * @param string $url
     * @return bool|string
     * @throws SiteGrabberException
     */
    public function getUrlContent(string $url)
    {
        $url = (strpos($this->domain, $url) === false) ? sprintf('%s%s%s', $this->domainPrefix, $this->domain, $url) : $url;

        try {
            $content = mb_strtolower(file_get_contents($url));
        } catch (\Exception $e) {
            throw new SiteGrabberException(sprintf("Error get content for url: %s ->%s", $url, $e->getMessage()));
        }

        return ($this->isValidUrlContent($content)) ? $content : null;
    }

    /**
     * True if html or xml document
     * @param string $content
     * @return bool
     */
    protected function isValidUrlContent(string $content): bool
    {
        $pattern = '/<html|<xml/';

        return (bool)preg_match($pattern, $content);
    }

    /**
     * @param string $content
     * @internal param array $parsersArray
     */
    public function parsePageContent(string $content): void
    {
        $urlsArray = $this->linkParser->parseLink($content);
        $keywordsArray = $this->keywordParser->parseKeyword($content);

        if (!empty($urlsArray)) {
            $this->saveUrls($urlsArray);
        }
        if (!empty($keywordsArray)) {
            $this->saveKeywords($keywordsArray);
        }
    }

    protected function saveUrls(array $urlsArray)
    {
        foreach ($urlsArray as $url) {
            try {
                $result = $this->urlService->addUrl($url, $this->domain);
                if ($result) {
                    Log::info(sprintf("Domain %s. Add url %s",
                        $this->domain,
                        $url
                    ));
                }
            } catch (\Exception $e) {
                Log::error(sprintf("Domain %s. Error add url %s with message %s",
                        $this->domain,
                        $url,
                        $e->getMessage()
                    )
                );
            }
        }
    }

    /**
     * @param array $keywordsArray
     * @throws \App\Core\Lang\Exception\AddKeywordException
     */
    protected function saveKeywords(array $keywordsArray)
    {
        $domainId = $this->domainInfo->id;
        $lang = $this->domainInfo->getLang();

        foreach ($keywordsArray as $keyword) {

            try {
                $result = $this->keywordService->addKeyword($keyword, $lang, $domainId);
                if ($result) {
                    Log::info(sprintf("Domain %s. Add keyword %s",
                        $this->domain,
                        $keyword
                    ));
                }
            } catch (\Exception $e) {
                Log::error(sprintf("Domain %s. Lang %s. Error add keyword %s with message %s",
                        $this->domain,
                        $lang,
                        $keyword,
                        $e->getMessage()
                    )
                );
            }
        }
    }
}