<?php

namespace Tests\Feature;

use App\Core\Domain\Service\DomainService;
use Tests\TestCase;

class DomainServiceTest extends TestCase
{
    protected $domain;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetAllDomains()
    {
        $domainService = $this->app->make(DomainService::class);
        $allDomains = $domainService->getAllDomains();

        $this->assertTrue((bool)\count($allDomains));
    }

    /**
     * @dataProvider getUrlsForDomainDataProvider
     * @param string $domain
     * @param bool $onlyProcessed
     */
    public function testGetUrlsForDomain(string $domain, bool $onlyProcessed)
    {
        $domainService = $this->app->make(DomainService::class);
        $urlsArray = $domainService->getDomainUrls($domain, $onlyProcessed);

        $this->assertTrue((bool)\count($urlsArray));
    }

    public function getUrlsForDomainDataProvider(): array
    {
        return [
            ['test1.ru', true],
            ['test1.ru', false],
        ];
    }

    public function testAddDeleteDomain()
    {
        $domainName = uniqid('test-domain_', true);
        $lang = 'ru';

        $domainService = $this->app->make(DomainService::class);
        $domainService->addDomain($domainName, $lang);
        $domainInfo = $domainService->getDomainInfo($domainName);
        $domainService->removeDomain($domainName);

        $this->assertTrue(isset($domainInfo));
    }

    public function testGetDomainWithNotPassedUrl()
    {
        $domainService = $this->app->make(DomainService::class);

        $domain = $domainService->getDomainWithNotProcessedUrl();
        $this->assertTrue((bool)$domain);
    }
}
