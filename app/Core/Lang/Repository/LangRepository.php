<?php

namespace App\Core\Lang\Repository;


use App\Core\Lang\Model\Lang;

class LangRepository
{
    /**
     * @param string $name
     */
    public function addLang(string $name): void
    {
        $lang = new Lang();
        $lang->name = $name;
        $lang->save();
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function getLangIdByName(string $name)
    {
        return Lang::where('name', $name)->get();
    }
}