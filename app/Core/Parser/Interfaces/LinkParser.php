<?php

namespace App\Core\Parser\Interfaces;


interface LinkParser
{
    public function parseLink(string $content): array;
}