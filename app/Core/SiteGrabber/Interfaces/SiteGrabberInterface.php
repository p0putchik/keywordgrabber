<?php

namespace App\Core\SiteGrabber\Interfaces;

interface SiteGrabberInterface
{
    public function parseUrls(array $urlsArray = null);
}