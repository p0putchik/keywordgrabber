<?php

namespace App\Core\Url\Model;

use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    public function domain()
    {
        return $this->belongsTo('App\Core\Domain\Model\Domain');
    }
}
