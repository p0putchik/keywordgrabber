<?php

namespace App\Core\Parser\LinkParsers;


use App\Core\Parser\Exception\LinkParserException;
use App\Core\Parser\Interfaces\LinkParser;
use Illuminate\Support\Facades\Config;

class BasicLinkParser implements LinkParser
{
    /**
     * @var string
     */
    protected $domain;
    /**
     * @var string
     */
    protected $domainPrefix;

    /** Link invalid if consist one of this pattern
     * @var array
     */
    protected $invalidLinkPatterns = [];

    /**
     * BasicLinkParser constructor.
     * @param $domain
     * @param $domainPrefix
     */
    public function __construct(string $domain, string $domainPrefix)
    {
        $this->domain = $domain;
        $this->domainPrefix = $domainPrefix;
        $this->init();
    }

    public function init()
    {
        $domain = str_replace('.','_', $this->domain);
//        $this->setInvalidLinkPatterns('/\.(js|css|jpg|gif|png|doc|tif|xml)$/');
        $configPath = sprintf('domaingrabber.%s.urlParser.invalidLinkPatterns', $domain);
        $defaultConfigPath = 'domaingrabber.default.urlParser.invalidLinkPatterns';
        $invalidLinkPatternsArray = !empty(Config::get($configPath)) ? Config::get($configPath) : Config::get($defaultConfigPath);
        $this->setInvalidLinkPatterns($invalidLinkPatternsArray);
    }

    /**
     * @param string $content
     * @return array
     */
    public function parseLink(string $content): array
    {
        $linksToDomain = $this->getLinksToDomain($content);
        $linksToDomain = $this->removeInvalidLinks($linksToDomain);

        return $linksToDomain;
    }

    /**
     * @param string $content
     * @return array
     * @throws LinkParserException
     */
    protected function getLinksToDomain(string $content)
    {
        if (empty($content)) {
            throw new LinkParserException("Empty content");
        }

        $domainPrefix = str_replace('/', '\/', $this->domainPrefix);
        $domain = str_replace('.', '\.', $this->domain);
        $pattern = sprintf('/href=\"(%s|\/)%s(.*?)"/', $domainPrefix, $domain);
        preg_match_all($pattern, $content, $matches);

        if (!count($matches)) {
            throw new LinkParserException(sprintf("Can't find links in content: %s", $content));
        }

        $returnLinksArray = [];
        foreach ($matches[2] as $match) {
            if (!empty($match)) {
                $returnLinksArray[] = $match;
            }
        }

        return array_unique($returnLinksArray);
    }

    /**
     * @param array $linksArray
     * @return array
     * @throws LinkParserException
     */
    protected function removeInvalidLinks(array $linksArray): array
    {
        if (empty($this->getInvalidLinkPatterns())) {
            throw new LinkParserException('Empty invalid link pattern');
        }

        $returnLinkArray = [];
        foreach ($linksArray as $link) {
            $canAdd = true;
            foreach ($this->getInvalidLinkPatterns() as $pattern) {
                if (preg_match($pattern, $this->removeParamsFromLink($link))) {
                    $canAdd = false;
                    continue;
                }
            }
            if ($canAdd) {
                $returnLinkArray[] = $link;
            }
        }

        return $returnLinkArray;
    }

    /**
     * @return mixed
     */
    public function getInvalidLinkPatterns(): array
    {
        return $this->invalidLinkPatterns;
    }

    /**
     * @param string|array $invalidLinkPattern
     * @internal param string $invalidLinkPatterns
     */
    public function setInvalidLinkPatterns($invalidLinkPattern)
    {
        if (is_array($invalidLinkPattern)){
            $this->invalidLinkPatterns = array_merge($this->invalidLinkPatterns, $invalidLinkPattern);
            return;
        }
        $this->invalidLinkPatterns[] = $invalidLinkPattern;
    }

    /**
     * @param string $link
     * @return string
     * @throws LinkParserException
     */
    protected function removeParamsFromLink(string $link)
    {
        if (!str_contains($link, '?')) {
            return $link;
        }

        $tmpArray = explode('?', $link);
        if (empty($tmpArray[0])) {
            throw new LinkParserException(sprintf('Something wrong with link %s', $link));
        }

        return $tmpArray[0];
    }
}