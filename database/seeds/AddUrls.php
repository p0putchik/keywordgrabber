<?php

use App\Core\Domain\Model\Domain;
use Illuminate\Database\Seeder;

class AddUrls extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $domains = DB::table('domains')->get();

        for($i=0;$i<5;$i++){
            $domainId = $domains[rand(0, count($domains)-1)]->id;

            DB::table('urls')->insert([
                'name' => uniqid(),
                'domain_id' => $domainId,
                'created_at' => date("Y-m-d")
            ]);
        }
    }
}
