<?php

namespace App\Core\Url\Dto;


use Illuminate\Database\Eloquent\Model;

class UrlDto
{
    protected $id;
    protected $name;
    protected $domainId;
    protected $processed;

    /**
     * UrlDto constructor.
     * @param int $id
     * @param string $name
     * @param int $domainId
     * @param bool $processed
     * @internal param string $lang
     * @internal param int $langId
     */
    public function __construct(int $id, string $name, int $domainId, bool $processed)
    {
        $this->id = $id;
        $this->name = $name;
        $this->domainId = $domainId;
        $this->processed = $processed;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getDomainId(): int
    {
        return $this->domainId;
    }

    /**
     * @param int $domainId
     */
    public function setDomainId(int $domainId)
    {
        $this->domainId = $domainId;
    }

    /**
     * @return bool
     */
    public function isProcessed(): bool
    {
        return $this->processed;
    }

    /**
     * @param bool $processed
     */
    public function setProcessed(bool $processed)
    {
        $this->processed = $processed;
    }

}