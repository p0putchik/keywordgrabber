<?php

return [
    'urlProcessCount' => 100,

    'default' =>[
        'urlParser' => [
            'invalidLinkPatterns' =>[
                '/\.(js|css|jpg|gif|png|doc|tif|xml)$/'
            ]
        ],
        'keywordParser' => [
            'validContentMarkerPatterns' => [
            ],
            'keywordExtractPatterns' => [
                '/meta name=\"keywords\" content=\"(.*?)"/'
            ]
        ]
    ],
    'muziazenysfotografiemi_logdown_com' =>[
        'keywordParser' => [
            'keywordExtractPatterns' => [
                '/<title>(.*?)«/'
            ]
        ]
    ],
    'trucktv_eu' =>[
        'keywordParser' => [
            'keywordExtractPatterns' => [
                '/>(.*?)<\/h1/'
            ]
        ]
    ],
    'mobivie_eu' =>[
        'keywordParser' => [
            'keywordExtractPatterns' => [
                '/<h1 class=\"fvp-single-post-title\">(.*?)<\/h1>/'
            ]
        ]
    ],
    'econetwork_eu' =>[
        'keywordParser' => [
            'keywordExtractPatterns' => [
                '/<h1 class=\"fvp-single-post-title\">(.*?)<\/h1>/'
            ]
        ]
    ],
    'inovinky_eu' =>[
        'keywordParser' => [
            'keywordExtractPatterns' => [
                '/h3>(.*?)<\/h3/'
            ]
        ]
    ],
];