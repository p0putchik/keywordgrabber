<?php

namespace App\Core\Parser\KeywordParsers;


use App\Core\Parser\Exception\KeywordParserException;
use App\Core\Parser\Interfaces\KeywordParser;
use Illuminate\Support\Facades\Config;

class BasicKeywordParser implements KeywordParser
{
    protected $domain;

    /**
     * If match one of the pattern then content is valid
     * @var array
     */
    protected $validContentMarkerPatterns = [];
    /**
     * Use this patterns for extracting keywords
     * @var array
     */
    protected $keywordExtractPatterns = [];

    /**
     * BasicKeywordParser constructor.
     * @internal param array $keywordExtractPatterns
     * @param string $domain
     */
    public function __construct(string $domain)
    {
        $this->domain = $domain;
        $this->init();
    }

    public function init()
    {
        $domain = str_replace('.','_', $this->domain);
        $configPath = sprintf('domaingrabber.%s.keywordParser.validContentMarkerPatterns', $domain);
        $defaultConfigPath = 'domaingrabber.default.keywordParser.validContentMarkerPatterns';
        $validContentMarkerPatterns = !empty(Config::get($configPath)) ? Config::get($configPath) : Config::get($defaultConfigPath);
        $this->setValidContentMarkerPatterns($validContentMarkerPatterns);

        $configPath = sprintf('domaingrabber.%s.keywordParser.keywordExtractPatterns', $domain);
        $defaultConfigPath = 'domaingrabber.default.keywordParser.keywordExtractPatterns';
        $keywordExtractPatterns = !empty(Config::get($configPath)) ? Config::get($configPath) : Config::get($defaultConfigPath);
        $this->setKeywordExtractPatterns($keywordExtractPatterns);
        //$this->setKeywordExtractPatterns('/meta name=\"keywords\" content=\"(.*?)"/');
    }

    /**
     * @param string $content
     * @return array
     */
    public function parseKeyword(string $content): array
    {
        if (!$this->isContentValid($content)) {
            //invalid content
            return [];
        }

        return $this->extractKeywords($content);
    }

    /**
     * @param string $content
     * @return bool
     */
    protected function isContentValid(string $content): bool
    {
        if (empty($this->getValidContentMarkerPatterns())) {
            return true;
        }

        foreach ($this->getValidContentMarkerPatterns() as $pattern) {
            if (preg_match($pattern, $content)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $content
     * @return array
     * @throws KeywordParserException
     */
    protected function extractKeywords(string $content): array
    {
        if (empty($this->getKeywordExtractPatterns())) {
            throw new KeywordParserException("Empty patterns for keyword extractor");
        }

        $keywordsArray = [];
        foreach ($this->getKeywordExtractPatterns() as $pattern) {
            preg_match_all($pattern, $content, $matches);
            if (isset($matches[1][0])) {
                $keywordsArray = array_merge($keywordsArray, explode(",", $matches[1][0]));
            }
        }

        return $keywordsArray;
    }

    /**
     * @return array
     */
    public function getValidContentMarkerPatterns(): ?array
    {
        return $this->validContentMarkerPatterns;
    }

    /**
     * @param array|string $validContentMarkerPatterns
     */
    public function setValidContentMarkerPatterns($validContentMarkerPatterns)
    {
        if (is_array($validContentMarkerPatterns)) {
            $this->validContentMarkerPatterns = array_merge($this->validContentMarkerPatterns, $validContentMarkerPatterns);
            return;
        }
        $this->validContentMarkerPatterns[] = $validContentMarkerPatterns;
    }

    /**
     * @return array
     */
    public function getKeywordExtractPatterns(): array
    {
        return $this->keywordExtractPatterns;
    }

    /**
     * @param array|string $keywordExtractPatterns
     */
    public function setKeywordExtractPatterns($keywordExtractPatterns)
    {
        if (is_array($keywordExtractPatterns)) {
            $this->keywordExtractPatterns = array_merge($this->keywordExtractPatterns, $keywordExtractPatterns);
            return;
        }

        $this->keywordExtractPatterns[] = $keywordExtractPatterns;
    }
}