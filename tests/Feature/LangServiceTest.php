<?php

namespace Tests\Feature;

use App\Core\Lang\Service\LangService;
use Tests\TestCase;

class LangServiceTest extends TestCase
{
    /**
     * @dataProvider getLangNamesDataProvider
     * @param string $langName
     * @param int $langId
     * @internal param string $domain
     * @internal param bool $onlyProcessed
     */
    public function testGetLangIdByName(string $langName, $langId)
    {
        $langService = $this->app->make(LangService::class);
        $id = $langService->getLangIdByName($langName);

        $this->assertEquals($id, $langId);
    }

    /**
     * @return array
     */
    public function getLangNamesDataProvider(): array
    {
        return [
            ['ru', 1],
            ['en', 2],
        ];
    }
}
