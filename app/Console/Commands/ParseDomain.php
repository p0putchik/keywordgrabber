<?php

namespace App\Console\Commands;

use App\Core\Parser\Factory\BasicParsersFactory;
use App\Core\Parser\Interfaces\AbstractParsersFactory;
use App\Core\SiteGrabber\Service\BasicSiteGrabber;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;

class ParseDomain extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'grabber:parseDomain {domain : domain name} {--c|count=1 : iteration count (default = 1)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse Domain';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $domain = $this->argument('domain');
        $count = $this->option('count');

        App::bind(AbstractParsersFactory::class, BasicParsersFactory::class);
        $basicGrabber = App::make(BasicSiteGrabber::class);
        $basicGrabber->init($domain);

        $bar = $this->output->createProgressBar($count);
        try {
            for ($i = 0; $i < $count; $i++) {
                $basicGrabber->parseUrls();
                $bar->advance();
            }
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }

        $bar->finish();
    }
}
