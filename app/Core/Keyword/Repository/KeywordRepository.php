<?php

namespace App\Core\Keyword\Repository;

use App\Core\Keyword\Model\Keyword;

class KeywordRepository
{
    /**
     * @param string $lang
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getKeywordsForLang(string $lang)
    {
        return Keyword::with(['lang' => function ($query) use ($lang) {
            $query->where('name', $lang);
        }])->get();
    }

    /**
     * @param string $keyword
     * @param string $lang
     * @return bool
     */
    public function isExistKeyword(string $keyword, string $lang):bool
    {
        $keywordObject = Keyword::where('name', $keyword)->with(['lang' => function ($query) use ($lang) {
            $query->where('name', $lang);
        }])->get();

        return isset($keywordObject[0]) ? true : false;
    }

    /**
     * @param string $keyword
     * @param int $langId
     * @param int $domainId
     */
    public function addKeyword(string $keyword, int $langId, int $domainId): void
    {
        $keywordModel = new Keyword();
        $keywordModel->name = $keyword;
        $keywordModel->lang_id = $langId;
        $keywordModel->domain_id = $domainId;
        $keywordModel->save();
    }

    /**
     * @param string $keyword
     * @param string $lang
     */
    public function deleteKeyword(string $keyword, string $lang): void
    {
        Keyword::with(['lang' => function ($query) use ($lang) {
            $query->where('name', $lang);
        }])->where('name', $keyword)->delete();
    }

    /**
     * @param string $domain
     */
    public function deleteKeywordsForDomain(string $domain): void
    {
        Keyword::with(['domain' => function ($query) use ($domain) {
            $query->where('name', $domain);
        }])->delete();
    }

    /**
     * @param string $lang
     */
    public function deleteKeywordsForLang(string $lang): void
    {
        Keyword::with(['lang' => function ($query) use ($lang) {
            $query->where('name', $lang);
        }])->delete();
    }
}