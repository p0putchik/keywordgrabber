<?php

namespace App\Core\Url\Repository;

use App\Core\Url\Model\Url;

class UrlRepository
{
    /**
     * @param string $urlName
     * @param int $domainId
     * @return bool
     */
    public function addUrl(string $urlName, int $domainId): bool
    {
        $urlModel = new Url();
        $urlModel->name = $urlName;
        $urlModel->domain_id = $domainId;

        return $urlModel->save();
    }

    /**
     * @param string $urlName
     * @param int $domainId
     * @return bool
     */
    public function isUrlExist(string $urlName, int $domainId): bool
    {
        $urlObject = Url::where('name', $urlName)->where('domain_id', $domainId)->get();

        return isset($urlObject[0]) ? true : false;
    }

    /**
     * @return mixed
     */
    public function getDomainWithNotProcessedUrls()
    {
        $url = Url::whereNotNull('domain_id')
            ->where('processed', false)
            ->with('domain')
            ->inRandomOrder()
            ->first();

        return $url;
    }

    /**
     * @param int $domainId
     * @param int $count
     * @return mixed
     */
    public function getNotProcessedUrls(int $domainId, int $count = 1)
    {
        $urls = Url::whereNotNull('domain_id')
            ->where([
                ['processed', false],
                ['domain_id', $domainId]
            ])
            ->inRandomOrder()
            ->limit($count)
            ->get();

        return $urls;
    }

    public function markUrlAsProcessed(string $urlName, int $domainId)
    {
        Url::where([
            ['domain_id', $domainId],
            ['name', $urlName]
        ])
            ->update(['processed' => true]);
    }
}